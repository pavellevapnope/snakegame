// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Food.generated.h"

class UStaticMeshComponent;
class USphereComponent;

UCLASS()
class MYPROJECT2_API AFood : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFood();


	UPROPERTY(BlueprintReadWrite)
		UStaticMeshComponent* MeshComponent;

	UPROPERTY(VisibleAnywhere, Category = "Trigger")
		USphereComponent* SphereTrigger;

	UPROPERTY()
		float RadiusSphere;

	UPROPERTY()
		float SpeedRotation;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = "?")
	virtual void Interact1(AActor* Interactor, bool bIsHead);
	

	



};
