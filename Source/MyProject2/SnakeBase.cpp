// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Food_Star.h"
#include "Interactable.h"



// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	ElementRot = 90;
	MovementSpeed = 0.36f;
	LastMoveDirection = EMovementDirection::UP;
}


// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();

	SetActorTickInterval(0.36f);

	AddSnakeElement(3);
}



void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Move();
}



void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(0, 0, 52);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;  
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);

		bDoOnceVisible = true;
		SnakeElements.Last()->SetActorHiddenInGame(true);
		

		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
	}
	
	SnakeElements.Last()->SetActorEnableCollision(0);

	if (bDoOnce)
	{
		bDoOnce = false;

		SnakeElements[0]->SetHidden(false);

		SnakeElements[1]->SetHidden(false);

		SnakeElements[2]->SetHidden(false);
		SnakeElements[2]->SetActorEnableCollision(1);
		
	}

	//GetWorld()->GetTimerManager().SetTimer(OnVisibleHandle, this, &ASnakeBase::SetVisible, 2, false);
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	FRotator RotationHead;
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		RotationHead.Yaw = -180;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		RotationHead.Yaw = 0;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		RotationHead.Yaw = -90;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		RotationHead.Yaw = 90;
		break;
	}

	
	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		FVector CurrentLocation = CurrentElement->GetActorLocation();
		FRotator MovementRotate;
		if (CurrentLocation.X < PrevLocation.X)
		{
			MovementRotate.Yaw = -180;
			SnakeElements[i]->SetActorRotation(MovementRotate);
		}
		else if (CurrentLocation.X > PrevLocation.X)
		{
			MovementRotate.Yaw = 0;
			SnakeElements[i]->SetActorRotation(MovementRotate);
		}
		else if (CurrentLocation.Y < PrevLocation.Y)
		{
			MovementRotate.Yaw = -90;
			SnakeElements[i]->SetActorRotation(MovementRotate);
		}
		else if (CurrentLocation.Y > PrevLocation.Y)
		{
			MovementRotate.Yaw = 90;
			SnakeElements[i]->SetActorRotation(MovementRotate);
		}
		SnakeElements[0]->SetActorRotation(RotationHead);
		CurrentElement->SetActorLocation(PrevLocation);
	}

	if (bDoOnceVisible)
	{
		bDoOnceVisible = false;
		SnakeElements.Last()->SetActorHiddenInGame(false);
		SnakeElements.Last()->SetActorEnableCollision(1);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{

	if (IsValid(OverlappedElement))
	{
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		if(InteractableInterface)
		{
			InteractableInterface->Interact1(this, bIsFirst);
		}
		
	}
}

void ASnakeBase::TimerEffect()
{
	GetWorldTimerManager().ClearTimer(OnActivedTimerHandle);
	SetActorTickInterval(0.36f);
}

void ASnakeBase::StopEffect()
{
	GetWorld()->GetTimerManager().SetTimer(OnActivedTimerHandle, this, &ASnakeBase::TimerEffect, 3.31f, false);
}

void ASnakeBase::SetRotate()
{
	FRotator MovementRotate;
	
	switch (LastMovementRotation)
	{
		
		case EMovementRotation::FORWARD:
			MovementRotate.Yaw = 0;
			break;
		case EMovementRotation::BACK:
			MovementRotate.Yaw = -180;
			break;
		case EMovementRotation::LEFTROT:
			MovementRotate.Yaw = -90;
			break;
		case EMovementRotation::RIGHTROT:
			MovementRotate.Yaw = 90;
			break;
	}

	SnakeElements[0]->SetActorRotation(MovementRotate);

}






