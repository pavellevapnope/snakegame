// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Food.h"
#include "Interactable.h"
#include "Food_Star.generated.h"

/**
 * 
 */


UCLASS()
class MYPROJECT2_API AFood_Star : public AFood
{
	GENERATED_BODY()
	

public:
	AFood_Star();

	void Interact1(AActor* Interactor, bool bIsHead) override;

};
