// Fill out your copyright notice in the Description page of Project Settings.


#include "Food_Apple.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"


// Sets default values
AFood_Apple::AFood_Apple()
{

	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Apple"));

	static ConstructorHelpers::FObjectFinder<UStaticMesh> Apple(TEXT("/Game/Apple.Apple"));
	if (Apple.Succeeded())
	{
		MeshComponent->SetStaticMesh(Apple.Object);
		MeshComponent->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
		MeshComponent->SetWorldScale3D(FVector(1.0f));
	};
	SpeedRotation = 1.f;
}

// Called when the game starts or when spawned
void AFood_Apple::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood_Apple::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AddActorWorldRotation(FRotator(0, SpeedRotation, 0));
}

void AFood_Apple::Interact1(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			Destroy();
		}
	}
}


