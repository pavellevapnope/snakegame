// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UENUM()
enum class EMovementRotation
{
	FORWARD,
	BACK,
	LEFTROT,
	RIGHTROT
};

UCLASS()
class MYPROJECT2_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
		float ElementSize;

	UPROPERTY(EditDefaultsOnly)
		int ElementRot;

	UPROPERTY(EditDefaultsOnly)
		float ElementSizeX;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY()
		EMovementDirection LastMoveDirection;

	UPROPERTY()
		EMovementRotation LastMovementRotation;

	UPROPERTY(EditDefaultsOnly)
		float MovementSpeed;

	UPROPERTY()
		int Rotation;

	bool bDoOnce = true;

	bool bDoOnceVisible = true;

	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		void AddSnakeElement(int ElementsNum = 1);

	UFUNCTION(BlueprintCallable)
		void Move();

	UFUNCTION()
		void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);

	void TimerEffect();
	FTimerHandle OnActivedTimerHandle;

	UFUNCTION()
		void StopEffect();

	UFUNCTION()
		void SetRotate();

};
