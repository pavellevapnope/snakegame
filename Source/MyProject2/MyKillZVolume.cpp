// Fill out your copyright notice in the Description page of Project Settings.


#include "MyKillZVolume.h"
#include "SnakeBase.h"

void AMyKillZVolume::Interact1(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->Destroy();
		}
	}
}
