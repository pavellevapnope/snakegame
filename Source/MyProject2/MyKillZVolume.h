// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/KillZVolume.h"
#include "Interactable.h"
#include "MyKillZVolume.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECT2_API AMyKillZVolume : public AKillZVolume, public IInteractable
{
	GENERATED_BODY()

		void Interact1(AActor* Interactor, bool bIsHead);
};
