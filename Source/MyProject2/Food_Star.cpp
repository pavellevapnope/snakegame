// Fill out your copyright notice in the Description page of Project Settings.


#include "Food_Star.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"

AFood_Star::AFood_Star()
{
	static ConstructorHelpers::FObjectFinder<UStaticMesh> Star(TEXT("/Game/GlowingStar.GlowingStar"));

	MeshComponent->SetStaticMesh(Star.Object);
	MeshComponent->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
	MeshComponent->SetWorldScale3D(FVector(1.0f));
}



void AFood_Star::Interact1(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->SetActorTickInterval(0.5749f);
			Snake->AddSnakeElement();
			Snake->StopEffect();
			Destroy();
		}
	}
}


