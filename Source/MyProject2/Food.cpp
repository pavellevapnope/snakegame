// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "SnakeElementBase.h"

// Sets default values
AFood::AFood()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	RadiusSphere = 5.0f;
	SphereTrigger = CreateDefaultSubobject<USphereComponent>(TEXT("TriggerVol"));
	
	SphereTrigger->InitSphereRadius(RadiusSphere);
	SphereTrigger->SetCollisionProfileName("Trigger");
	RootComponent = SphereTrigger;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Banana"));
	MeshComponent->AttachTo(RootComponent);

	static ConstructorHelpers::FObjectFinder<UStaticMesh> Banana(TEXT("/Game/Megascans/3D_Assets/Banana_vfendgyiw/Banana_LOD0_vfendgyiw.Banana_LOD0_vfendgyiw"));

	MeshComponent->SetStaticMesh(Banana.Object);
	MeshComponent->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
	MeshComponent->SetWorldScale3D(FVector(1.0f));

	SpeedRotation = 1.f;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AddActorWorldRotation(FRotator(0, SpeedRotation, 0));
}


void AFood::Interact1(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			Destroy();
		}
	}
}















