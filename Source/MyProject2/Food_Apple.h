// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Food_Apple.generated.h"

class UStaticMeshComponent;
class USphereComponent;


UCLASS()
class MYPROJECT2_API AFood_Apple : public AActor, public IInteractable
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	AFood_Apple();

	UPROPERTY(BlueprintReadWrite)
		UStaticMeshComponent* MeshComponent;

	UPROPERTY()
		float SpeedRotation;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		void Interact1(AActor* Interactor, bool bIsHead) override;
	
};
